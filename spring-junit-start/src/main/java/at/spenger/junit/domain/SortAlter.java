package at.spenger.junit.domain;

import java.util.Comparator;

public class SortAlter implements Comparator<Person> {


	 @Override
	public int compare(Person p1, Person p2) {
		 
			return p1.getBirthday().toString().compareTo(p2.getBirthday().toString());
	}
}